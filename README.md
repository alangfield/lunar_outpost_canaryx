# Lunar_Outpost_CanaryX

# Thesis
Lunar Outpost is an advanced technology company with a focus on developing technologies that
have both Earth and space applications. The “Canary” product line is an ongoing Lunar Outpost
project. The “Canary” devices monitor air quality in real-time and transfer the data to an online
dashboard. The focus of this field session was to integrate multiple communication protocols into
the “Canary” product line. The finished product is a fully functioning “Canary-X” module that
contains a variety of communication protocols including Ethernet, Wi-Fi, and Cellular. The
“Canary-X” module samples air quality data and works with the communication protocols to
send data across the network once per minute. The data is received by a server/dashboard
showing details and analysis of the data.

# Functional requirements:
Software and hardware development for a variety of communication protocols including
Bluetooth, Ethernet, Wi-Fi, and cellular connectivity. The system is designed as a
software/hardware interface specified to work in parallel using sensors with multi-purpose
capabilities. Hardware is built to gather data real-time and transmit via one of the three internet
connection methods described above. The hardware’s main purpose is to relay specific air
quality data including CO, temperature, humidity, CO2, pressure, wind speed and direction,
VOCs, and many other gases.

# Non-functional requirements:
● The data transmitted is consistent across protocols.
● The code is written in the Arduino IDE.
● The code is written in C++ with Arduino libraries
● OneDrive is used for version control and project documentation
● Data is communicated using HTTPS POST requests
● Data is transmitted using the most efficient communication protocol, in order of Ethernet,
Wi-Fi, and Cellular.
● Develop for the:
○ Adafruit Feather M4 Express (ATSAMD51 Cortex M4)
○ Adafruit Ethernet Featherwing
○ Sparkfun SARA-R4 LTE Module
○ ESP8266 Wi-Fi chip
○ BME280 sensor
○ ZOEM8Q GPS

